package com.example.mylayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.net.URI;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void OnclickOk (View view){
        Button btn_ok = (Button) findViewById(R.id.btn1);
        Intent intent = new Intent(MainActivity.this,Profile.class);
        startActivity(intent);
    }
}
